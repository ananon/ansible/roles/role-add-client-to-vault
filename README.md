# role-add-client-to-vault
* This repository contains a role related to HashiCorp Vault.
* Prerequisites
	* `OC` installed on the server.
	*  `Ansible` installed on the server.
	* `ansible-modules-hashivault` Python module installed on the server.

* The role is used when we would like to add a client to the HashiCorp Vault.
* The role creates:
    * a "K/V 2" secret engine for the client
    * admin and read-only policies for the secret engine
    * a Vault user for the client and assigns the admin policy to the user


| Variable | Description | Type | Default | Required |
| ------------- |:-------------:| -----:|:-------------:| -----:|
| vault_url    | The URL of the Vault | String | "" | True
| vault_username     | Username to the Vault      |   String | "" | True
| vault_password | Password to the Vault      |    String | "" | True
| client_name | Name of the client      |    String | "" | True
